package com.example.test_springboot_java17;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestSpringbootJava17Application {

	public static void main(String[] args) {
		SpringApplication.run(TestSpringbootJava17Application.class, args);
	}

}
