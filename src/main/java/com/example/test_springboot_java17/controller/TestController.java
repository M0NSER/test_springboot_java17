package com.example.test_springboot_java17.controller;

import com.example.test_springboot_java17.controller.dto.ListElement;
import com.example.test_springboot_java17.controller.dto.ListParams;
import com.example.test_springboot_java17.controller.dto.ResponseDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping
public class TestController {
    @Value("${example-env:default value}")
    private String exampleEnv;

    @GetMapping
    public ResponseEntity<?> sayHallo() {
        return ResponseEntity.ok(new ResponseDto(exampleEnv));
    }

    @GetMapping("/list")
    public ResponseEntity<?> getList(@Valid ListParams listParams){
        List<ListElement> list = new ArrayList<>();

        for (int i = 0; i < listParams.getSize(); i++){
            list.add(new ListElement(
                    (int) ((Math.random() * (1000)) + 0),
                    UUID.randomUUID().toString()
            ));
        }

        return ResponseEntity.ok(list);
    }
}
