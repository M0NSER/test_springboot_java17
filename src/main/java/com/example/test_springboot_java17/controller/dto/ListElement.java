package com.example.test_springboot_java17.controller.dto;

public class ListElement {
    private int randomId;

    private String randomMessage;

    public ListElement(int randomId, String randomMessage) {
        this.randomId = randomId;
        this.randomMessage = randomMessage;
    }

    public int getRandomId() {
        return randomId;
    }

    public void setRandomId(int randomId) {
        this.randomId = randomId;
    }

    public String getRandomMessage() {
        return randomMessage;
    }

    public void setRandomMessage(String randomMessage) {
        this.randomMessage = randomMessage;
    }
}
