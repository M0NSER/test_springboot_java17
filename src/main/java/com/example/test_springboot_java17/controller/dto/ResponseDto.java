package com.example.test_springboot_java17.controller.dto;

public class ResponseDto {
    private String status = "ok";

    private String message = "Hello world, I am Java SpringBoot first app on this domain :)";

    private String exampleEnv;

    public ResponseDto(String exampleEnv) {
        this.exampleEnv = exampleEnv;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getExampleEnv() {
        return exampleEnv;
    }

    public void setExampleEnv(String exampleEnv) {
        this.exampleEnv = exampleEnv;
    }
}
