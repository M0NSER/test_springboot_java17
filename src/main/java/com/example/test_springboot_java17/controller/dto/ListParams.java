package com.example.test_springboot_java17.controller.dto;


import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class ListParams {
    @Min(0)
    @Max(value = 500)
    private int size = 5;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
